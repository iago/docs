# Instalando Ubuntu 21.10 Server
En este apartado se explica la instalación de Ubuntu 21.10 de Servidor en un sistema de 64 bits (en este caso en una máquina virtual) y en una raspberry pi 4 (arm64)
## Instalación en arm64
### Descargar y grabar la imagen
Para instalar ubuntu en la raspberry, deberemos de descargar la imagen de la página de ubuntu, como utilizo una raspberry, descargaré su imagen en particular, que se encuentra [aquí](https://ubuntu.com/download/raspberry-pi)
Para grabar la imagen, lo más fácil es utilizar una utilidad gráfica, en mi caso, la utilidad de discos de gnome.
![Utilidad de discos de gnome](gnome-disks.png)
Para grabar la imagen haz clic en `Restaurar imagen de disco...`, selecciona el disco, el archivo de la imagen e introduce tu contraseña.
![Restaurar imagen de disco - Discos de gnome](gnome-disks-restore-disk-image.png)
### Arrancar la raspberry
Ahora que la imagen ya está en la tarjeta, el siguiente paso es insertarla en la raspberry, conectar el cable de ethernet y alimentarla. La raspberry arrancará al recibir la alimentación.
## Instalación en amd64

## Configurando Ubuntu 21.10
Ahora que ya hemos instalado el sistema operativo, lo siguiente es instalar algunas utilidades y herramientas.
### Instalando ddclient
```
sudo apt install ddclient
```
### Instalando nginx
```
sudo apt install nginx
```
### Instalando certbot
```
sudo apt install python3-certbot-nginx
```
### Utilizando cron
