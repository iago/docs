# Alojando servicios con docker

En esta sección se encuentra toda la documentación relacionada con cómo configuro y ejecuto servicios con docker en mi servidor local.

## Servicios que ejecuto
- [Nextcloud](https://nextcloud.com): Tu propia nube, con nextcloud puedes sincronizar tus contactos y calendarios, además de compartir archivos.
- [Vaultwarden](https://github.com/dani-garcia/vaultwarden): Bitwarden es un gestor de contraseñas que permite sincronizar todos los dispositivos. Vaultwarden es una implementación del servidor de bitwarden en Rust que incluye todas las funciones sin necesidad de ningún pago.
- [Coder](https://github.com/coder/code-server): VS-Code en la web. Te permite programar desde cualquier ordenador con un navegador web.
- [Synapse](https://matrix.org/docs/projects/server/synapse): Servidor del protocolo de mensajería abierto [matrix](https://matrix.org).
- [Prosody](https://prosody.im): Servidor de XMPP. Fácil de instalar y de configurar. No tienen una imagen de docker oficial.

## Antes de empezar
Lo primero es tener un servidor sobre el que correr los servicios. Si solo quieres hacer pruebas, puedes utilizar una máquina virtual, si quieres que estos servicios estén funcionando continuamente, puedes utilizar una raspberry pi, ya que su consumo eléctrico es muy bajo.
Una vez que tengas el sistema instalado, necesitarás configurar un dominio con un servidor de dns dinámico y un proxy ([nginx](https://nginx.org)) con los certificados de SSL instalados.
