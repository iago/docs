# Documentación de Delthia
En este subdominio se aloja toda mi documentación.

La documentación se agrupa en guías relacionadas dentro de un mismo tema; los temas se pueden navegar en la barra superior. Los temas son los siguientes:

- [Android](Android/): Guías relacionadas con aplicaciones libres para Android.
- [Docker](Docker/): Guías relacionandas con configurar y ejecutar servicios en docker, pero también con configurar y mantener un servidor en general.

??? warning "Documentación en construcción"
	Esta página todavía está en construcción y tiene contenido y seccciones incompletas. Progreso:

	- [x] Configuración del sitio
	- [ ] Secciones:
		* [ ] Android
			* [ ] Introducción
			* [ ] Contenido
		* [ ] Docker
			* [ ] Introducción
			*	[ ] Contenido
	- [ ] Organización del contenido

!!! info "Licencia y fuente del contenido"
	El contenido de [este subdominio](https://docs.delthia.com) se aloja en [codeberg pages](https://codeberg.page). La fuente se encuentra [en este repositorio](https://codeberg.org/iago/docs). La licencia de toda la documentación, su fuente y todos los recursos que se encuentren en el repositorio o que se puedan acceder desde este subdominio se encuentran bajo la licencia [Creative Commons CompartirIgual 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)
